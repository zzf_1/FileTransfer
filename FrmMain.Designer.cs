﻿namespace FileTransfer
{
    partial class FrmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.label1 = new System.Windows.Forms.Label();
            this.txtIPAddress = new System.Windows.Forms.TextBox();
            this.btnStartMonitor = new System.Windows.Forms.Button();
            this.btnEndMonitor = new System.Windows.Forms.Button();
            this.txtRemoteAddress = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnTestConnect = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPartFileLengh = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lab1111 = new System.Windows.Forms.Label();
            this.labServerFileBase = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labSocketInfo = new System.Windows.Forms.TextBox();
            this.txtLocalPort = new System.Windows.Forms.TextBox();
            this.btnSelectFlod = new System.Windows.Forms.Button();
            this.btnPartFileLenght = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.progressbarMain = new System.Windows.Forms.ProgressBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labMsg = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSendFile = new System.Windows.Forms.Button();
            this.benRecive = new System.Windows.Forms.Button();
            this.txtClientFile = new System.Windows.Forms.TextBox();
            this.txtServerFile = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "监听IP:";
            // 
            // txtIPAddress
            // 
            this.txtIPAddress.Location = new System.Drawing.Point(120, 29);
            this.txtIPAddress.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtIPAddress.Name = "txtIPAddress";
            this.txtIPAddress.Size = new System.Drawing.Size(196, 27);
            this.txtIPAddress.TabIndex = 1;
            this.txtIPAddress.Text = "127.0.0.1";
            // 
            // btnStartMonitor
            // 
            this.btnStartMonitor.Location = new System.Drawing.Point(57, 132);
            this.btnStartMonitor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnStartMonitor.Name = "btnStartMonitor";
            this.btnStartMonitor.Size = new System.Drawing.Size(94, 39);
            this.btnStartMonitor.TabIndex = 2;
            this.btnStartMonitor.Text = "开启监听";
            this.btnStartMonitor.UseVisualStyleBackColor = true;
            this.btnStartMonitor.Click += new System.EventHandler(this.btnStartMonitor_Click);
            // 
            // btnEndMonitor
            // 
            this.btnEndMonitor.Enabled = false;
            this.btnEndMonitor.Location = new System.Drawing.Point(213, 132);
            this.btnEndMonitor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEndMonitor.Name = "btnEndMonitor";
            this.btnEndMonitor.Size = new System.Drawing.Size(112, 39);
            this.btnEndMonitor.TabIndex = 2;
            this.btnEndMonitor.Text = "断开";
            this.btnEndMonitor.UseVisualStyleBackColor = true;
            this.btnEndMonitor.Click += new System.EventHandler(this.btnEndMonitor_Click);
            // 
            // txtRemoteAddress
            // 
            this.txtRemoteAddress.Location = new System.Drawing.Point(158, 46);
            this.txtRemoteAddress.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRemoteAddress.Name = "txtRemoteAddress";
            this.txtRemoteAddress.Size = new System.Drawing.Size(287, 27);
            this.txtRemoteAddress.TabIndex = 4;
            this.txtRemoteAddress.Text = "127.0.0.1:9008";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(62, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "远程地址:";
            // 
            // btnTestConnect
            // 
            this.btnTestConnect.Location = new System.Drawing.Point(473, 45);
            this.btnTestConnect.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTestConnect.Name = "btnTestConnect";
            this.btnTestConnect.Size = new System.Drawing.Size(125, 39);
            this.btnTestConnect.TabIndex = 2;
            this.btnTestConnect.Text = "测试连接";
            this.btnTestConnect.UseVisualStyleBackColor = true;
            this.btnTestConnect.Click += new System.EventHandler(this.btnTestConnect_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPartFileLengh);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lab1111);
            this.groupBox1.Controls.Add(this.labServerFileBase);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.labSocketInfo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtLocalPort);
            this.groupBox1.Controls.Add(this.txtIPAddress);
            this.groupBox1.Controls.Add(this.btnStartMonitor);
            this.groupBox1.Controls.Add(this.btnSelectFlod);
            this.groupBox1.Controls.Add(this.btnPartFileLenght);
            this.groupBox1.Controls.Add(this.btnEndMonitor);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(977, 201);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "监听配置";
            // 
            // txtPartFileLengh
            // 
            this.txtPartFileLengh.Location = new System.Drawing.Point(701, 29);
            this.txtPartFileLengh.Margin = new System.Windows.Forms.Padding(4);
            this.txtPartFileLengh.Name = "txtPartFileLengh";
            this.txtPartFileLengh.Size = new System.Drawing.Size(93, 27);
            this.txtPartFileLengh.TabIndex = 6;
            this.txtPartFileLengh.Text = "1024";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(806, 33);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "KB";
            // 
            // lab1111
            // 
            this.lab1111.AutoSize = true;
            this.lab1111.Location = new System.Drawing.Point(606, 32);
            this.lab1111.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lab1111.Name = "lab1111";
            this.lab1111.Size = new System.Drawing.Size(84, 20);
            this.lab1111.TabIndex = 5;
            this.lab1111.Text = "数据包设置";
            // 
            // labServerFileBase
            // 
            this.labServerFileBase.AutoSize = true;
            this.labServerFileBase.Location = new System.Drawing.Point(347, 84);
            this.labServerFileBase.Name = "labServerFileBase";
            this.labServerFileBase.Size = new System.Drawing.Size(63, 20);
            this.labServerFileBase.TabIndex = 4;
            this.labServerFileBase.Text = "file info";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "服务器文件夹位置:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(387, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "监听端口:";
            // 
            // labSocketInfo
            // 
            this.labSocketInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.labSocketInfo.Location = new System.Drawing.Point(350, 141);
            this.labSocketInfo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labSocketInfo.Name = "labSocketInfo";
            this.labSocketInfo.ReadOnly = true;
            this.labSocketInfo.Size = new System.Drawing.Size(312, 20);
            this.labSocketInfo.TabIndex = 1;
            this.labSocketInfo.Text = "socket info";
            // 
            // txtLocalPort
            // 
            this.txtLocalPort.Location = new System.Drawing.Point(483, 29);
            this.txtLocalPort.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtLocalPort.Name = "txtLocalPort";
            this.txtLocalPort.Size = new System.Drawing.Size(76, 27);
            this.txtLocalPort.TabIndex = 1;
            this.txtLocalPort.Text = "9008";
            // 
            // btnSelectFlod
            // 
            this.btnSelectFlod.Location = new System.Drawing.Point(213, 74);
            this.btnSelectFlod.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSelectFlod.Name = "btnSelectFlod";
            this.btnSelectFlod.Size = new System.Drawing.Size(112, 39);
            this.btnSelectFlod.TabIndex = 2;
            this.btnSelectFlod.Text = "选择文件夹";
            this.btnSelectFlod.UseVisualStyleBackColor = true;
            this.btnSelectFlod.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnPartFileLenght
            // 
            this.btnPartFileLenght.Location = new System.Drawing.Point(836, 24);
            this.btnPartFileLenght.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPartFileLenght.Name = "btnPartFileLenght";
            this.btnPartFileLenght.Size = new System.Drawing.Size(112, 39);
            this.btnPartFileLenght.TabIndex = 2;
            this.btnPartFileLenght.Text = "确定";
            this.btnPartFileLenght.UseVisualStyleBackColor = true;
            this.btnPartFileLenght.Click += new System.EventHandler(this.btnPartFileLenght_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtRemoteAddress);
            this.groupBox2.Controls.Add(this.btnTestConnect);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 201);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(977, 104);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "远程配置";
            // 
            // progressbarMain
            // 
            this.progressbarMain.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressbarMain.Location = new System.Drawing.Point(0, 683);
            this.progressbarMain.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.progressbarMain.Name = "progressbarMain";
            this.progressbarMain.Size = new System.Drawing.Size(977, 29);
            this.progressbarMain.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labMsg);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.btnSendFile);
            this.panel1.Controls.Add(this.benRecive);
            this.panel1.Controls.Add(this.txtClientFile);
            this.panel1.Controls.Add(this.txtServerFile);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 305);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(977, 378);
            this.panel1.TabIndex = 8;
            // 
            // labMsg
            // 
            this.labMsg.AutoSize = true;
            this.labMsg.Location = new System.Drawing.Point(348, 354);
            this.labMsg.Name = "labMsg";
            this.labMsg.Size = new System.Drawing.Size(0, 20);
            this.labMsg.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(42, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(814, 220);
            this.label6.TabIndex = 6;
            this.label6.Text = resources.GetString("label6.Text");
            // 
            // btnSendFile
            // 
            this.btnSendFile.Location = new System.Drawing.Point(528, 76);
            this.btnSendFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSendFile.Name = "btnSendFile";
            this.btnSendFile.Size = new System.Drawing.Size(216, 39);
            this.btnSendFile.TabIndex = 5;
            this.btnSendFile.Text = "发送文件到服务器";
            this.btnSendFile.UseVisualStyleBackColor = true;
            this.btnSendFile.Click += new System.EventHandler(this.btnSendFile_Click);
            // 
            // benRecive
            // 
            this.benRecive.Location = new System.Drawing.Point(528, 20);
            this.benRecive.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.benRecive.Name = "benRecive";
            this.benRecive.Size = new System.Drawing.Size(216, 39);
            this.benRecive.TabIndex = 5;
            this.benRecive.Text = "从服务器接收文件";
            this.benRecive.UseVisualStyleBackColor = true;
            this.benRecive.Click += new System.EventHandler(this.benRecive_Click);
            // 
            // txtClientFile
            // 
            this.txtClientFile.Location = new System.Drawing.Point(172, 82);
            this.txtClientFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtClientFile.Name = "txtClientFile";
            this.txtClientFile.ReadOnly = true;
            this.txtClientFile.Size = new System.Drawing.Size(331, 27);
            this.txtClientFile.TabIndex = 1;
            // 
            // txtServerFile
            // 
            this.txtServerFile.Location = new System.Drawing.Point(168, 26);
            this.txtServerFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtServerFile.Name = "txtServerFile";
            this.txtServerFile.Size = new System.Drawing.Size(334, 27);
            this.txtServerFile.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(42, 76);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(111, 39);
            this.button2.TabIndex = 2;
            this.button2.Text = "选择文件";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(72, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "文件名称";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(977, 712);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.progressbarMain);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "文件传输助手";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Label label1;
        private TextBox txtIPAddress;
        private Button btnStartMonitor;
        private Button btnEndMonitor;
        private TextBox txtRemoteAddress;
        private Label label2;
        private Button btnTestConnect;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Label labServerFileBase;
        private Label label3;
        private Button btnSelectFlod;
        private ProgressBar progressbarMain;
        private Panel panel1;
        private Label label5;
        private Button benRecive;
        private Button btnSendFile;
        private Label label6;
        private Button button2;
        private TextBox txtClientFile;
        private Label label7;
        private TextBox txtLocalPort;
        private TextBox labSocketInfo;
        private Label labMsg;
        private TextBox txtServerFile;
        private TextBox txtPartFileLengh;
        private Label lab1111;
        private Button btnPartFileLenght;
        private Label label4;
    }
}