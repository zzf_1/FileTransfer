using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;

namespace FileTransfer
{
    public partial class FrmMain : Form
    {
        private static object objLock = new object();
        private long FilePartLength = 1048576;//1048576; 文件分片大小
        private int msgLength = 1024;//消息长度 
        Socket _ServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        AutoResetEvent AutoResetEvent = new AutoResetEvent(true);
        public FrmMain()
        {
            InitializeComponent();
            labServerFileBase.Text = AppDomain.CurrentDomain.BaseDirectory;
        }
        /// <summary>
        /// 开启监听
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStartMonitor_Click(object sender, EventArgs e)
        {
            if (labServerFileBase.Text == "file info")
            {
                MessageBox.Show("请选择文件位置");
                return;
            }
            Task.Factory.StartNew(() =>
            {
                try
                {
                    _ServerSocket.Bind(new IPEndPoint(IPAddress.Parse(txtIPAddress.Text.Trim()), int.Parse(txtLocalPort.Text.Trim())));
                    _ServerSocket.Listen(100000);
                    btnStartMonitor.Invoke(() =>
                    {
                        btnEndMonitor.Enabled = true;
                        btnStartMonitor.Enabled = false;
                        btnPartFileLenght.Enabled = false;
                        btnSelectFlod.Enabled = false;
                        labSocketInfo.Text = $"地址:{txtIPAddress.Text.Trim()}:{txtLocalPort.Text.Trim()}";
                    });
                    while (btnEndMonitor.Enabled == true)
                    {
                        _ServerSocket.BeginAccept(new AsyncCallback(Listen_Callback), _ServerSocket);
                        AutoResetEvent.WaitOne();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"监听端口{txtLocalPort.Text.Trim()}失败，请更换端口后重试，错误详情：{ex.Message}");
                }
                finally
                {

                }
            });
        }
        private void Listen_Callback(IAsyncResult ar)
        {
            try
            {
                var listens = ar.AsyncState as Socket;
                var s = listens?.EndAccept(ar);
                byte[] recvBytes = new byte[FilePartLength+msgLength];//1m+1k;
                s?.ReceiveAsync(recvBytes, SocketFlags.None).ContinueWith(res =>
                {
                    if (res.Status == TaskStatus.Faulted)
                    {
                        return;
                    }
                    if (!Directory.Exists(Path.Combine(labServerFileBase.Text, "Temp")))
                    {
                        Directory.CreateDirectory(Path.Combine(labServerFileBase.Text, "Temp"));
                    }
                    ArraySegment<byte> bytesMsg = new ArraySegment<byte>(recvBytes, 0, msgLength);
                    string msg = Encoding.UTF8.GetString(bytesMsg).TrimEnd('\0');
                    SendFileDataInfo sendFileDataInfo = msg.ToObject<SendFileDataInfo>();
                    string fileName = sendFileDataInfo.FileName;
                    if (sendFileDataInfo.Type == "Send")
                    {
                        if (sendFileDataInfo.Option == "Default")
                        {

                            int index = sendFileDataInfo.Index;
                            long length = sendFileDataInfo.Length;
                            string filePath = Path.Combine(labServerFileBase.Text, "Temp", $"{fileName}_{index}");

                            if (sendFileDataInfo.Length > 0)
                            {
                                ArraySegment<byte> bytesData = new ArraySegment<byte>(recvBytes, msgLength, int.Parse(sendFileDataInfo.Length.ToString()));
                                var data = bytesData.ToArray();
                                if (!data.ComputerHash().Equals(sendFileDataInfo.HashCode))
                                {
                                    s.Send(Encoding.UTF8.GetBytes("-1"));//接收完成返回索引
                                }
                                else
                                {
                                    using (FileStream fs = new FileStream(filePath, FileMode.Create))
                                    {
                                        fs.Write(data);
                                        fs.Flush();
                                    }
                                    s.Send(Encoding.UTF8.GetBytes(index.ToString()));//接收完成返回索引
                                }
                            }
                        }
                        else if (sendFileDataInfo.Option == "Cache")
                        {
                            //对比返回需要上传的片段
                            string[] fileFullNames = Directory.GetFiles(Path.Combine(labServerFileBase.Text, "Temp"), $"{sendFileDataInfo.FileName}_*");
                            //读取文件区内容反序列化得到任务列表
                            ArraySegment<byte> bytesData = new ArraySegment<byte>(recvBytes, msgLength, int.Parse(FilePartLength.ToString()));
                            var data = bytesData.ToArray();
                            string sendFileStr = Encoding.UTF8.GetString(data).TrimEnd('\0');
                            SendTask sendTask = sendFileStr.ToObject<SendTask>();
                            if (sendTask == null)
                            {
                                MessageBox.Show("数据包设置过小，请调整后重试");
                                btnEndMonitor_Click(new object(),new EventArgs());
                            }
                            Regex reg = new Regex("_\\d{1,}$");
                            if (fileFullNames.Length > 0)
                            {
                                for (int i = 0; i < fileFullNames.Length; i++)
                                {
                                    var indexStr = reg.Match(fileFullNames[i]);
                                    if (indexStr.Success)
                                    {
                                        var index = int.Parse(indexStr.Value.TrimStart('_'));

                                        if (sendTask.PartComplated.Keys.Contains(index)&&File.ReadAllBytes(fileFullNames[i]).ComputerHash().Equals(sendTask.PartHash[index]))
                                        {
                                            sendTask.PartComplated[index] = true;
                                        }
                                    }
                                }
                            }
                            byte[] uploadedMap = Encoding.UTF8.GetBytes(sendTask.ToJson());
                            s.Send(uploadedMap);
                        }
                        else
                        {
                            string fileRealPath = Path.Combine(labServerFileBase.Text, $"{fileName}");
                            var count = sendFileDataInfo.Count;
                            using (FileStream fs = new FileStream(fileRealPath, FileMode.Create))
                            {
                                for (int i = 0; i < count; i++)
                                {

                                    var tempbytes = File.ReadAllBytes(Path.Combine(labServerFileBase.Text, "Temp", $"{fileName}_{i}"));
                                    
                                    fs.Write(tempbytes);
                                    fs.Flush();
                                }
                                
                            }
                            for (int i = 0; i <= count; i++)
                            {
                                File.Delete(Path.Combine(labServerFileBase.Text, "Temp", $"{fileName}_{i}"));
                            }
                            s.Send(Encoding.UTF8.GetBytes("OK"));//接收完成返回索引
                        }
                    }
                    else
                    {
                        if (sendFileDataInfo.Option == "InitGetFile")
                        {
                            var resFile = getFileSendTask(Path.Combine(labServerFileBase.Text, $"{sendFileDataInfo.FileName}"));
                            s.Send(resFile);
                        }
                        else
                        {
                            lock (objLock)
                            {
                                var fileBytes = getFileData(Path.Combine(labServerFileBase.Text, $"{sendFileDataInfo.FileName}"), sendFileDataInfo.Index);
                                s.Send(fileBytes.Result);
                            }
                        }
                    }
                });
                AutoResetEvent.Set();
            }
            catch
            {}
        }
        /// <summary>
        /// 结束监听
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEndMonitor_Click(object sender, EventArgs e)
        {
            try
            {
                _ServerSocket.Shutdown(SocketShutdown.Both);
            }
            catch
            {
            }
            _ServerSocket.Close();
            _ServerSocket.Dispose();
            _ServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            btnEndMonitor.Enabled = false;
            btnStartMonitor.Enabled = true;
            btnPartFileLenght.Enabled = true;
            btnSelectFlod.Enabled = true;
        }
        /// <summary>
        /// 根据索引和文件路径获取待发送的信息
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private (byte[] Result, long Count) getFileData(string filepath, int index)
        {
            string fileName = Path.GetFileName(filepath);
            using (FileStream fs = new FileStream(filepath, FileMode.Open))
            {
                var lastLength = fs.Length % FilePartLength;//最后一段的长度
                var count = fs.Length / FilePartLength;//总段数
                if (lastLength > 0)
                {
                    count += 1;
                }
                long byteLength = FilePartLength;
                if (index == count - 1)//最后一段
                {
                    byteLength = lastLength == 0 ? FilePartLength : int.Parse(lastLength.ToString());
                }
                byte[] filebt = new byte[byteLength];
                fs.Seek(index * FilePartLength, SeekOrigin.Begin);
                fs.Read(filebt, 0, int.Parse(byteLength.ToString()));
                SendFileDataInfo fileDataInfo = new SendFileDataInfo()
                {
                    Type = "Send",
                    Count = count,
                    FileName = fileName,
                    Index = index,
                    Length = byteLength,
                    HashCode = filebt.ComputerHash()
                };
                var msgbyte = Encoding.UTF8.GetBytes(fileDataInfo.ToJson());
                byte[] sendByte = new byte[byteLength + msgLength];
                msgbyte.CopyTo(sendByte, 0);
                filebt.CopyTo(sendByte, msgLength);
                return (sendByte, count);
            }
        }
        /// <summary>
        /// 获取所有需要获取的文件段
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        private byte[] getFileSendTask(string filepath)
        {
            string fileName = Path.GetFileName(filepath);
            SendTask send = new SendTask();
            send.FileName = fileName;
            if (!File.Exists(filepath))
            {
                send.Error = "服务器不存在文件";
            }
            else
            {
                using (FileStream fs = new FileStream(filepath, FileMode.Open))
                {
                    var lastLength = fs.Length % FilePartLength;//最后一段的长度
                    var count = fs.Length / FilePartLength;//总段数
                    if (lastLength > 0)
                    {
                        count += 1;
                    }
                    long byteLength = FilePartLength;

                    for (int index = 0; index < count; index++)
                    {
                        if (index == count - 1)//最后一段
                        {
                            byteLength = lastLength == 0 ? FilePartLength : int.Parse(lastLength.ToString());
                        }
                        byte[] filebt = new byte[byteLength];
                        fs.Seek(index * FilePartLength, SeekOrigin.Begin);
                        fs.Read(filebt, 0, int.Parse(byteLength.ToString()));
                        send.PartComplated.Add(index, false);
                        send.PartHash.Add(index, filebt.ComputerHash());
                    }
                }
            }
            return Encoding.UTF8.GetBytes(send.ToJson());
        }

        /// <summary>
        /// 文件缓存询问消息
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        private byte[] getFileCacheRequest(string filepath,Action<string,int,int>? process)
        {
            string fileName = Path.GetFileName(filepath);
            SendTask send = new SendTask();
            long count = 0;
            send.FileName = fileName;
            if (!File.Exists(filepath))
            {
                send.Error = "不存在文件";
            }
            else
            {
                using (FileStream fs = new FileStream(filepath, FileMode.Open))
                {
                    var lastLength = fs.Length % FilePartLength;//最后一段的长度
                    count = fs.Length / FilePartLength;//总段数
                    if (lastLength > 0)
                    {
                        count += 1;
                    }
                    long byteLength = FilePartLength;

                    for (int index = 0; index < count; index++)
                    {
                        if (index == count - 1)//最后一段
                        {
                            byteLength = lastLength == 0 ? FilePartLength : int.Parse(lastLength.ToString());
                        }
                        byte[] filebt = new byte[byteLength];
                        fs.Seek(index * FilePartLength, SeekOrigin.Begin);
                        fs.Read(filebt, 0, int.Parse(byteLength.ToString()));
                        send.PartComplated.Add(index, false);
                        send.PartHash.Add(index, filebt.ComputerHash());
                        process?.Invoke($"计算文件Hash值{(index / count)*100}%",index,int.Parse(count.ToString()));
                    }
                }
            }
            SendFileDataInfo fileDataInfo = new SendFileDataInfo()
            {
                Type = "Send",
                Option = "Cache",
                Count = count,
                FileName = fileName,
                Index = -1,
                Length = -1
            };
            var msgbyte = Encoding.UTF8.GetBytes(fileDataInfo.ToJson());
            byte[] sendByte = new byte[FilePartLength + msgLength];
            msgbyte.CopyTo(sendByte, 0);
            var sendTaskByte = Encoding.UTF8.GetBytes(send.ToJson());
            sendTaskByte.CopyTo(sendByte, msgLength);
            return sendByte;
        }
        /// <summary>
        /// 构建合并文件请求
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private byte[] buildMerge(string filePath)
        {
            string fileName = Path.GetFileName(filePath);
            using (FileStream fs = new FileStream(filePath, FileMode.Open))
            {
                var lastLength = fs.Length % FilePartLength;//最后一段的长度
                var count = fs.Length / FilePartLength;//总段数
                if (lastLength > 0)
                {
                    count += 1;
                }
                SendFileDataInfo fileDataInfo = new SendFileDataInfo()
                {
                    Type = "Send",
                    Option = "Merge",
                    Count = count,
                    FileName = fileName,
                    Index = -1,
                    Length = -1
                };
                var msgbyte = Encoding.UTF8.GetBytes(fileDataInfo.ToJson());
                byte[] sendByte = new byte[msgLength];
                msgbyte.CopyTo(sendByte, 0);
                return sendByte;
            }
        }
        /// <summary>
        /// 发送Socket消息
        /// </summary>
        /// <param name="remotingAddress">远程地址</param>
        /// <param name="sendData">发送的数据</param>
        /// <param name="recvtes">接收的响应缓存区</param>
        /// <param name="reciveFunc">接收的回调</param>
        private void SendSocketMsg(string remotingAddress, byte[] sendData, byte[] recvtes, Action<Task<int>> reciveFunc)
        {
            var _ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _ClientSocket.SetIPProtectionLevel(IPProtectionLevel.Unrestricted);
            _ClientSocket.Connect(IPEndPoint.Parse(remotingAddress));
            _ClientSocket.SendTimeout = int.MaxValue;
            _ClientSocket.SendAsync(sendData, SocketFlags.None).Wait();
            _ClientSocket.ReceiveAsync(recvtes, SocketFlags.None).ContinueWith(reciveFunc).Wait();
        }
        private void btnSendFile_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtRemoteAddress.Text))
            {
                MessageBox.Show("请输入远程地址");
                return;
            }
            Task.Factory.StartNew(() =>
            {
                progressbarMain.Invoke(() =>
                {
                    labMsg.Text = $"处理中...";
                });
                string filePath = txtClientFile.Text.Trim();
                //看看服务器有没有同名文件的缓存，如果有就返回剩余需要处理的任务
                var fileCacheReq = getFileCacheRequest(filePath, (msg, current, count) => {
                    this.progressbarMain.Invoke(() =>
                    {
                        labMsg.Text = msg;
                        progressbarMain.Maximum = count;
                        progressbarMain.Value = current;
                    });
                });
                SendTask sendTask = new SendTask();
                var fileCacheRecvBytes = new byte[FilePartLength];
                SendSocketMsg(txtRemoteAddress.Text.Trim(), fileCacheReq, fileCacheRecvBytes, res =>
                {
                    if (!res.IsFaulted)
                    {
                        //得到任务列表
                        string uploadedStr = Encoding.UTF8.GetString(fileCacheRecvBytes).TrimEnd('\0');
                        sendTask = uploadedStr.ToObject<SendTask>();
                    }
                });
                DateTime start = DateTime.Now;
                var count = sendTask.PartComplated.Where(item => item.Value == false).Count();
                while (sendTask.PartComplated.Where(item => item.Value == false).Count() > 0)
                {
                    var item = sendTask.PartComplated.FirstOrDefault(item => item.Value == false);
                    int index = item.Key;
                    var sendFileData = getFileData(filePath, index);

                    byte[] recvBytes = new byte[msgLength];
                    SendSocketMsg(txtRemoteAddress.Text.Trim(), sendFileData.Result, recvBytes, res =>
                    {
                        if (!res.IsFaulted)
                        {
                            int index = int.Parse(Encoding.UTF8.GetString(recvBytes));
                            if (index > -1)
                            {
                                sendTask.PartComplated[index] = true;
                            }
                        }
                    });
                    progressbarMain.Invoke(() =>
                    {
                        var dt = DateTime.Now - start;
                        var itemSize = long.Parse(this.txtPartFileLengh.Text) * 1024;
                        var r = (sendTask.PartComplated.Where(item => item.Value == true).Count() * 1024d) / dt.Seconds;
                        var barValue = Math.Round((double.Parse(sendTask.PartComplated.Where(item => item.Value == true).Count().ToString()) / double.Parse(count.ToString())) * 100, 2);
                        labMsg.Text = $"处理进度{barValue}% 速度  {Math.Round(r, 2)} kb/s";
                        progressbarMain.Maximum = count;
                        progressbarMain.Value = count-sendTask.PartComplated.Where(item => item.Value == false).Count();
                    });
                }


                //发送文件处理完毕合并文件请求
                var bytes = buildMerge(filePath);

                byte[] mrecvBytes = new byte[msgLength];
                SendSocketMsg(txtRemoteAddress.Text.Trim(), bytes, mrecvBytes, res =>
                {
                    if (!res.IsFaulted)
                    {
                        progressbarMain.Invoke(() =>
                        {
                            progressbarMain.Maximum = 100;
                            progressbarMain.Value = 0;
                            labMsg.Text = $"传输完成！";
                        });
                    }
                });
            });
        }
        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtClientFile.Text = openFileDialog.FileName;
            }
        }

        private void benRecive_Click(object sender, EventArgs e)
        {
            string errormsg = "请输入文件名称，该文件名称为服务器文件，如服务器 文件位置为E:/ServerFiles/test.txt. 服务器文件夹位置为E:/ServerFiles，这里需要需要填写的是test.txt";
            if (string.IsNullOrEmpty(txtServerFile.Text.Trim()))
            {
                MessageBox.Show(errormsg);
                return;
            }
            string fileName = Path.GetFileName(txtServerFile.Text.Trim());
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            var res = folderBrowserDialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                Task.Factory.StartNew(() =>
                {
                    progressbarMain.Invoke(() =>
                    {
                        labMsg.Text = $"处理中...";
                    });
                    DateTime start = DateTime.Now;
                    string dir = folderBrowserDialog.SelectedPath;
                    Directory.CreateDirectory(Path.Combine(dir, "Temp"));
                    //先看看本地的缓存文件，如果有就记录下来
                    string[] fileFullNames = Directory.GetFiles(Path.Combine(dir, "Temp"), $"{fileName}_*");
                    //发送我要某个文件的请求
                    var bytes = getReciveFileData(this.txtServerFile.Text.Trim());
                    byte[] recvBytes = new byte[FilePartLength];
                    SendSocketMsg(txtRemoteAddress.Text.Trim(), bytes, recvBytes, res =>
                    {
                        if (!res.IsFaulted)
                        {
                            if (!res.IsFaulted)
                            {
                                string msg = Encoding.UTF8.GetString(recvBytes);
                                SendTask sendTask = msg.TrimEnd('\0').ToObject<SendTask>();
                                if (sendTask == null)
                                {
                                    MessageBox.Show("文件过大,计算hash值长度超过限制，请修改数据包设置后重试");
                                    return;
                                }
                                if (sendTask.Error.Length > 0)
                                {
                                    MessageBox.Show(sendTask.Error);
                                    return;
                                }
                                //计算 已经缓存本地的文件
                                Regex reg = new Regex("_\\d{1,}$");
                                if (fileFullNames.Length > 0)
                                {
                                    for (int i = 0; i < fileFullNames.Length; i++)
                                    {
                                        var indexStr = reg.Match(fileFullNames[i]);
                                        if (indexStr.Success)
                                        {
                                            var index = int.Parse(indexStr.Value.TrimStart('_'));
                                            //计算文件hash值，一样的记录true
                                            if (File.ReadAllBytes(fileFullNames[i]).ComputerHash().Equals(sendTask.PartHash[index]))
                                            {
                                                sendTask.PartComplated[index] = true;
                                            }
                                        }
                                    }
                                }
                                
                                int count = sendTask.PartComplated.Where(item => item.Value == false).Count();
                                while (sendTask.PartComplated.Where(item => item.Value == false).Count() > 0)
                                {
                                    var item = sendTask.PartComplated.FirstOrDefault(item => item.Value == false);
                                    int index = item.Key;
                                    var reciveFileRequest = getReciveFileData(this.txtServerFile.Text.Trim(), index);
                                    byte[] reciveFiles = new byte[FilePartLength+msgLength];
                                    SendSocketMsg(txtRemoteAddress.Text.Trim(), reciveFileRequest, reciveFiles, res =>
                                    {
                                        if (!res.IsFaulted)
                                        {
                                            ArraySegment<byte> bytes = new ArraySegment<byte>(reciveFiles, 0, msgLength);
                                            string msg = Encoding.UTF8.GetString(bytes).TrimEnd('\0');
                                            SendFileDataInfo reciveFileDataInfo = msg.ToObject<SendFileDataInfo>();

                                            int index = reciveFileDataInfo.Index;
                                            long length = reciveFileDataInfo.Length;
                                            string filePath = Path.Combine(dir, "Temp", $"{reciveFileDataInfo.FileName}_{index}");

                                            ArraySegment<byte> fileData = new ArraySegment<byte>(reciveFiles, msgLength, int.Parse(length.ToString()));
                                            var fileDatabytes = fileData.ToArray();
                                            if (fileDatabytes.ComputerHash().Equals(sendTask.PartHash[index]))
                                            {
                                                using (FileStream fs = new FileStream(filePath, FileMode.Create))
                                                {
                                                    fs.Write(fileDatabytes);
                                                }
                                                sendTask.PartComplated[index] = true;
                                            }
                                        }
                                    });
                                    progressbarMain.Invoke(() =>
                                    {
                                        var dt = DateTime.Now - start;
                                        var itemSize = long.Parse(this.txtPartFileLengh.Text) * 1024;
                                        var r = (sendTask.PartComplated.Where(item => item.Value == true).Count() * 1024d) / dt.Seconds;
                                        var barValue = Math.Round((double.Parse(sendTask.PartComplated.Where(item => item.Value == true).Count().ToString()) / double.Parse(count.ToString())) * 100, 2);
                                        labMsg.Text = $"处理进度{barValue}% 速度  {Math.Round(r, 2)} kb/s";
                                        progressbarMain.Maximum = count;
                                        progressbarMain.Value = count - sendTask.PartComplated.Where(item => item.Value == false).Count();
                                    });
                                }
                                //本地合并文件
                                string fileRealPath = Path.Combine(dir, $"{txtServerFile.Text.Trim()}");
                                var total = sendTask.PartComplated.Count;
                                using (FileStream fs = new FileStream(fileRealPath, FileMode.Create))
                                {
                                    for (int i = 0; i < total; i++)
                                    {
                                        var tempbytes = File.ReadAllBytes(Path.Combine(dir, "Temp", $"{txtServerFile.Text.Trim()}_{i}"));
                                        fs.Write(tempbytes);
                                    }
                                }
                                for (int i = 0; i <= total; i++)
                                {
                                    File.Delete(Path.Combine(dir, "Temp", $"{txtServerFile.Text.Trim()}_{i}"));
                                }
                                progressbarMain.Invoke(() =>
                                {
                                    labMsg.Text = $"接收完成！";
                                });
                            }
                        }
                    });
                });
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private byte[] getReciveFileData(string fileName)
        {
            SendFileDataInfo sendFileDataInfo = new SendFileDataInfo()
            {
                FileName = fileName,
                Type = "Recive",
                Option = "InitGetFile"
            };
            var msgbyte = Encoding.UTF8.GetBytes(sendFileDataInfo.ToJson());
            byte[] sendByte = new byte[msgLength];
            msgbyte.CopyTo(sendByte, 0);
            return sendByte;
        }

        private byte[] getReciveFileData(string fileName, int index)
        {
            SendFileDataInfo sendFileDataInfo = new SendFileDataInfo()
            {
                FileName = fileName,
                Type = "Recive",
                Option = "Default",
                Index = index
            };
            var msgbyte = Encoding.UTF8.GetBytes(sendFileDataInfo.ToJson());
            byte[] sendByte = new byte[msgLength];
            msgbyte.CopyTo(sendByte, 0);
            return sendByte;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                labServerFileBase.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void btnTestConnect_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtRemoteAddress.Text))
            {
                MessageBox.Show("请输入远程地址");
                return;
            }
            Task.Factory.StartNew(() =>
            {
                try
                {
                    this.btnTestConnect.Invoke(() =>
                    {
                        btnTestConnect.Enabled = false;
                    });
                    var _ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    _ClientSocket.Connect(IPEndPoint.Parse(txtRemoteAddress.Text.Trim()));
                    MessageBox.Show("测试连接成功");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    this.btnTestConnect.Invoke(() =>
                    {
                        btnTestConnect.Enabled = true;
                    });
                }
            });
        }

        private void btnPartFileLenght_Click(object sender, EventArgs e)
        {
            long length = long.Parse(this.txtPartFileLengh.Text)* 1024;
            this.FilePartLength = length;
            this.txtPartFileLengh.Enabled = false;
            this.btnPartFileLenght.Enabled = false;
        }
    }
    /// <summary>
    /// 文件发送任务
    /// </summary>
    public class SendTask
    {
        public SendTask()
        {
            PartComplated = new Dictionary<int, bool>();
            PartHash = new Dictionary<int, string>();
        }
        /// <summary>
        /// 分段完成情况
        /// </summary>
        public Dictionary<int, bool> PartComplated { get; set; }
        /// <summary>
        /// 分段的hash值
        /// </summary>
        public Dictionary<int, string> PartHash { get; set; }
        public bool Complated
        {
            get
            {
                if (PartComplated.Where(item => item.Value == false).Count() == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; } = "";
        /// <summary>
        /// 错误
        /// </summary>
        public string Error { get; set; } = "";
    }

    public class SendFileDataInfo
    {
        /// <summary>
        /// 消息类型，Send Recive
        /// </summary>
        public string Type { get; set; } = "Send";//Recive
        /// <summary>
        /// 操作类型  Default Cache InitGetFiles
        /// </summary>
        public string Option { get; set; } = "Default";
        /// <summary>
        /// 总段数
        /// </summary>
        public long Count { get; set; }
        /// <summary>
        /// 当前索引
        /// </summary>
        public int Index { get; set; }
        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; } = "";
        /// <summary>
        /// 当前段长度
        /// </summary>
        public long Length { get; set; }
        /// <summary>
        /// 当前段 hashCode
        /// </summary>
        public string HashCode { get; set; } = "";
    }
}