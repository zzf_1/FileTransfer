﻿using System.Security.Cryptography;
using System.Text;
using System.Text.Json;

namespace FileTransfer
{
    public static class JsonOption
    {
        public static T ToObject<T>(this string source)
        {
            try
            {
                return JsonSerializer.Deserialize<T>(source);
            }
            catch
            {
                return default(T);
            }

        }
        public static string ToJson<T>(this T t)
        {
            return JsonSerializer.Serialize(t);
        }

        public static string ComputerHash(this byte[] bts)
        {
            HashAlgorithm hashAlgorithm = HashAlgorithm.Create(HashAlgorithmName.MD5.Name);
            return Convert.ToBase64String(hashAlgorithm.ComputeHash(bts)) ;
        }
    }
}
